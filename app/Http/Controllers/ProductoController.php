<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = Producto::latest('fecha_crea')->paginate(10);
        return view('producto.index', [
            "productos" => $productos,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $producto = new Producto;

        return view('producto.create', [
            'producto' => $producto,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $atributos = request()->validate([
            'nombre'       => 'required',
            'referencia'     => 'required',
            'precio'  => 'required',
            'peso'             => 'required',
            'categoria'  => 'required',
            'stock'  => 'required',
            'fecha_crea'  => 'required',
            'fecha_ultima_venta'  => 'required',
        ]);

        Producto::create([
            'nombre'   => $request->nombre,
            'referencia' => $request->referencia,
            'precio'  => $request->precio,
            'peso'             => $request->peso,
            'categoria'       => $request->categoria,
            'stock'   => $request->stock,
            'fecha_crea'    => $request->fecha_crea,
            'fecha_ultima_venta' => $request->fecha_ultima_venta,
        ]);

//        Order::create(request->all());
//        session()->flash('success', 'Registro creado exitosamente');
        return redirect('producto')->with('success', 'Producto registrado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show(Producto $producto)
    {
        return view('producto.show', [
            'producto' => $producto,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
        return view('producto.edit', [
            'producto' => $producto,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Producto $producto)
    {
        $atributos = request()->validate([
            'nombre'       => 'required',
            'referencia'     => 'required',
            'precio'  => 'required',
            'peso'             => 'required',
            'categoria'  => 'required',
            'stock'  => 'required',
            'fecha_crea'  => 'required',
            'fecha_ultima_venta'  => 'required',
        ]);

        $producto->update($atributos);

//        session()->flash('success', 'Registro actualizado exitosamente');
        return redirect('producto')->with('success', 'Producto actualizado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Producto $producto)
    {
        $producto->delete();
        return redirect('producto')->with('success', 'Producto eliminado exitosamente');
    }
}
