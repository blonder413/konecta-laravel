@extends('layouts.app')

@section('title', 'Detalle Producto')

@section('content')

<h1>Producto <strong>{{ $producto->nombre }}</strong></h1>

<table class='table table-hover'>
    <tr>
        <th class='table-info'>Nombre</th>
        <td>{{ $producto->nombre }}</td>
    </tr>
    <tr>
        <th class='table-info'>Referencia</th>
        <td>{{ $producto->referencia }}</td>
    </tr>
    <tr>
        <th class='table-info'>Precio</th>
        <td>${{ number_format($producto->precio, 0, ',', '.') }}</td>
    </tr>
    <tr>
        <th class='table-info'>Peso</th>
        <td>{{ $producto->peso }}</td>
    </tr>
    <tr>
        <th class='table-info'>Categoría</th>
        <td>{{ $producto->categoria }}</td>
    </tr>
    <tr>
        <th class='table-info'>Stock</th>
        <td>{{ $producto->stock }}</td>
    </tr>
    <tr>
        <th class='table-info'>Fecha Crea</th>
        <td>{{ $producto->fecha_crea }}</td>
    </tr>
    <tr>
        <th class='table-info'>Fecha última venta</th>
        <td>{{ $producto->fecha_ultima_venta }}</td>
    </tr>
</table>

@endsection
