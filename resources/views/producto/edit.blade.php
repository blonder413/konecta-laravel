@extends('layouts.app')

@section('title', 'Editar Producto')

@section('content')

<h1>Editar Producto <strong>{{ $producto->nombre }}</strong></h1>

<form action="{{ url('/producto/' . $producto->id) }}" method="post">
    @csrf
    {{ method_field('PATCH') }}
    @include('producto.form')
</form>

@endsection
