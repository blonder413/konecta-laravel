
<div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" placeholder="Nombre" value="{{ old('nombre',$producto->nombre) }}" class='form-control'>
            @error('nombre')
            <p class='alert-danger text-xs mt-1'>{{ $message }}</p>
            @enderror
        </div>
        <div class="form-group">
            <label for="referencia">Referencia</label>
            <input type="text" name="referencia" placeholder="Referencia" value="{{ old('referencia', $producto->referencia) }}" class='form-control'>
            @error('referencia')
            <p class='alert-danger text-xs mt-1'>{{ $message }}</p>
            @enderror
        </div>
        <div class="form-group">
            <label for="precio">Precio</label>
            <input type="text" name="precio" placeholder="Precio" value="{{ old('precio', $producto->precio) }}" class='form-control'>
            @error('precio')
            <p class='alert-danger text-xs mt-1'>{{ $message }}</p>
            @enderror
        </div>
        <div class="form-group">
            <label for="peso">Peso</label>
            <input type="text" name="peso" placeholder="Peso" value="{{ old('peso', $producto->peso) }}" class='form-control'>
            @error('peso')
            <p class='alert-danger text-xs mt-1'>{{ $message }}</p>
            @enderror
        </div>
        <div class="form-group">
            <label for="categoria">Categoría</label>
            <input type="text" name="categoria" placeholder="Categoría" value="{{ old('categoria', $producto->categoria) }}" class='form-control'>
            @error('categoria')
            <p class='alert-danger text-xs mt-1'>{{ $message }}</p>
            @enderror
        </div>
        <div class="form-group">
            <label for="stock">Stock</label>
            <input type="text" name="stock" placeholder="Stock" value="{{ old('stock', $producto->stock) }}" class='form-control'>
            @error('stock')
            <p class='alert-danger text-xs mt-1'>{{ $message }}</p>
            @enderror
        </div>
        <div class="form-group">
            <label for="fecha_crea">Fecha Crea</label>
            <input type="text" name="fecha_crea" placeholder="Fecha Crea" value="{{ old('fecha_crea', $producto->fecha_crea) }}" class='form-control'>
            @error('fecha_crea')
            <p class='alert-danger text-xs mt-1'>{{ $message }}</p>
            @enderror
        </div>
        <div class="form-group">
            <label for="fecha_ultima_venta">Fecha última venta</label>
            <input type="text" name="fecha_ultima_venta" placeholder="Fecha última venta" value="{{ old('fecha_ultima_venta', $producto->fecha_ultima_venta) }}" class='form-control'>
            @error('fecha_ultima_venta')
            <p class='alert-danger text-xs mt-1'>{{ $message }}</p>
            @enderror
        </div>
        <div class="form-group">
            <input type="submit" value="Enviar" name="enviar" class='form-control btn btn-success'>
        </div>
