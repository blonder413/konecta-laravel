@extends('layouts.app')

@section('title', 'Productos | Konecta')

@section('content')

<h1>Productos</h1>

<p><a href="{{ url('/producto/create') }}" class="btn btn-success">Crear</a></p>

<table class='table table-hover'>

    <tr>
        <th class='table-primary'>Nombre</th>
        <th class='table-primary'>Referencia</th>
        <th class='table-primary'>Precio</th>
        <th class='table-primary'>Peso</th>
        <th class='table-primary'>Categoria</th>
        <th class='table-primary'>Stock</th>
        <th class='table-primary'>Fecha crea</th>
        <th class='table-primary'>Facha ultima venta</th>
        <th class='table-primary'></th>
    </tr>

    @foreach ($productos as $producto)
    <tr>
        <td>{{ $producto->nombre }}</td>
        <td>{{ $producto->referencia }}</td>
        <td>${{ number_format($producto->precio, 0, ',', '.') }}</td>
        <td>{{ $producto->peso }}</td>
        <td>{{ $producto->categoria }}</td>
        <td>{{ $producto->stock }}</td>
        <td>{{ $producto->fecha_crea }}</td>
        <td>{{ $producto->fecha_ultima_venta }}</td>
        <td>
            <a href="{{ route('producto.show', $producto) }}" class='btn btn-secondary'>Ver</a><br>
            <a href="{{ route('producto.edit', $producto) }}" class='btn btn-primary'>Editar</a><br>
            <form action="{{ route('producto.destroy', $producto) }}" method="post">
                @csrf
                {{ method_field('DELETE') }}
                <input type="submit" value="Eliminar" onclick="return confirm('Realmente desea eliminar este registro?')" class='btn btn-danger'>
            </form>
    </tr>
    @endforeach
</table>

<?= $productos->links(); ?>
@endsection
