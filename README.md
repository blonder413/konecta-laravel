RREQUISITOS
------------

Los requisitos mínimos requeridos para este proyecto es un servidor web con soporte para PHP 5.6.0.


INSTALACION
------------

### Install via git

Si no tiene [Git](https://git-scm.com/), puede descargarlo
en [git-scm.com](https://git-scm.com/downloads).

Primero debe ubicarse en la carpeta raíz de su servidor web desde la consola de comandos.
Una vez allí puede instalar el proyecto usando el siguiente comando:

~~~
git clone https://gitlab.com/blonder413/konecta-laravel
~~~

### Instalar desde un archivo

Extraiga el archivo descargado desde [git](https://gitlab.com/blonder413/konecta-laravel) 
al directorio raiz de su servidor web.

### Instalar extensiones

Si no tiene [Composer](http://getcomposer.org/), puede instalarlo siguiendo las instrucciones
en [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

Una vez que tenga composer debe dirigirse a la carpeta del proyecto e instalar las extensiones

~~~
cd konecta-laravel
composer install
~~~

CONFIGURACIÓN
-------------

### Base de datos

Renombre el archivo `.env.example` como `.env` y modifíquelo con información real, por ejemplo:

```php
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=konecta
DB_USERNAME=root
DB_PASSWORD=123456
```

Una vez configurada la conexión a la base de datos, podemos proceder a ejecutar las migraciones, esto creará las tabla productos.

```
php artisan migrate
```

Opcionalmente puede ejecutar el sql desde una interfaz para MySQL

```
create table productos (
id int unsigned auto_increment primary key,
nombre varchar(100) not null,
referencia varchar(50) not null,
precio int not null,
peso int not null,
categoria varchar(100) not null,
stock int not null, -- Cantidad del producto en bodega
fecha_crea date not null,
fecha_ultima_venta date not null
)engine=innodb;
```

Si está usando GNU/Linux es probable que necesite darle permisos de lectura y escritura al directorio `storage`.

Una vez hecho esto tendrá acceso a la aplicación a través de la siguiente URL:

~~~
http://localhost/konecta-laravel/public/
~~~


<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[Many](https://www.many.co.uk)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
- **[OP.GG](https://op.gg)**
- **[CMS Max](https://www.cmsmax.com/)**
- **[WebReinvent](https://webreinvent.com/?utm_source=laravel&utm_medium=github&utm_campaign=patreon-sponsors)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
